let fsp = require("fs").promises;

function fileOperations(){
let readLipsumFile = fsp.readFile("./lipsum.txt","utf-8")
readLipsumFile
.then((data) => {
    console.log("lipsum.txt file readed successfully.")
    let upperCaseData = data.toUpperCase();
    return fsp.writeFile("./upperCaseData.txt",upperCaseData)
    })
.then(function(){
    console.log("Upper case data stored in the file called upperCaseData.txt");
    return fsp.writeFile("./allFileNames.txt","upperCaseData.txt")
})
.then(function(){
    console.log("upperCaseData.txt name saved in file allFIleNames.txt");
    return fsp.readFile("./upperCaseData.txt","utf-8")
})
.then(function(data){
    let lowerCaseData = data.toLowerCase().split(". ");
    return fsp.writeFile("./lowerCaseSplitData.txt",lowerCaseData)
})
.then(function(){
    console.log("Lower case data stored in the file called lowerCaseData.txt");
    return fsp.appendFile("./allFileNames.txt",",lowerCaseSplitData.txt")
})
.then(function(){
    console.log("lowerCaseData.txt name saved in file allFIleNames.txt")
    return fsp.readFile("./lowerCaseSplitData.txt","utf-8")
})
.then(function(data){
    let sortedData = data.split(" ").sort();
    return fsp.writeFile("sorteddataFile.txt",sortedData)
})
.then(function(){
    console.log("Sorted data stored in the file called lowerCaseData.txt");
    return fsp.appendFile("./allFileNames.txt",",sorteddataFile.txt");
})
.then(function(){
    console.log("lowerCaseData.txt name saved in file allFIleNames.txt")
    return fsp.readFile("./allFileNames.txt","utf-8")
})
.then(function(data){
    let fileNames = data.split(",");
    for(let i=0;i<=fileNames.length-1;i++){
        fsp.unlink(fileNames[i])
        console.log(fileNames[i]+" file deleted");
    }
})
.catch((err) => console.log(err))

return readLipsumFile;
}

module.exports = fileOperations;
