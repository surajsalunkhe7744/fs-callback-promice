var fs = require("fs");

function randomFilesGenerator(randomNum) {
  let createdFilesCount = 0;
  let createDir = fs.promises.mkdir("node");
  for (let i = 0; i < randomNum; i++) {
    createDir.then(function () {
      fs.promises
        .writeFile(
          `./node/file_${i}.json`,
          JSON.stringify({ abc: 1000, def: 2000, ghi: 3000 })
        )
        .then(function () {
          createdFilesCount += 1;
          console.log(`Created file ${i + 1} out of ${randomNum}`);
          if (createdFilesCount === randomNum) {
            console.log("Starting delete operation");
            for (let i = 0; i < randomNum; i++) {
              fs.promises.unlink(`./node/file_${i}.json`).then(function () {
                console.log(`Deleted file ${i + 1}`);
              });
            }
          }
        });
    });
  }
}

module.exports = randomFilesGenerator;
